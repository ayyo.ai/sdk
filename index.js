const ayyo = {};
import { Geoffrey } from './lib/Geoffrey.js';
import _config from './config/config.js'
import localStorage from './config/localStorage.json' assert {type: 'json'};
import _paintPixels from './lib/paint/paintPixels.js';
import text2PixelEvents from './lib/paint/text2PixelEvents.js';

ayyo.Bot = function ({ nickname, wsConnectionString, config, apiKey, gameId, gameMode, region, publicKey, ayyoKey, token }) {
  let self = this;
  self.middlewares = [];
  let client = new Geoffrey({
    config: config || _config,
    localStorage: localStorage,
    ayyoKey: ayyoKey,
    apiKey: apiKey,
    wsConnectionString: wsConnectionString,
    nickname: nickname,
    gameMode: gameMode, // enum, default FOREVER_WAR
    region: region, // enum, default Washington_DC, add ping test to auto-connect to region
    gameId: gameId, // custom game id connection string, optional
    publicKey: publicKey,
    token: token

  })
  for (let prop in client) {
    self[prop] = client[prop];
  }
  // TODO: only when using middlewares do have have this default gamestate listener
  /*
  self.on('gamestate', function(gamestate){
    let stack = [];
    let response = {
      inputs: {},
      end: function(){
        // If response.end() is called, immediately send the player_inputs
        self.sendInputs(response.inputs);
      }
    };
    //
    // Get current player from gamestate
    //
    self.G = {};
    gamestate.snapshot.state.forEach(function(state){
      //console.log('state', state.id, self.currentPlayer)
      if (state.id === self.currentPlayer) {
        self.G = state;
        gamestate.G = state;
      }
    });
    self.middlewares.forEach(function(middle){
      stack.unshift(middle);
    });
    function next () {
      if (stack.length) {
        let fn = stack.pop();
        fn(gamestate, response, next)
      } else {
        // nothing left in middleware stack
        // completed all middlewars, end response
        self.sendInputs(response.inputs);
      }
    };
    next();
  });
  */
  return self;
};

ayyo.Bot.prototype = {
  use: function (fn) {
    let self = this;
    console.log('using fn', fn)
    if (typeof fn !== 'function') {
      console.log(`Error: ${fn} is not a function.`)
      throw new Error('Must pass in function(gamestate, response, next)')
    }
    self.middlewares.push(fn);
    return this; // return reference to self to allow chaining of function calls
  },
  paintPixels: function (pixelsArray, options) {
    options = options || {};
    _paintPixels(pixelsArray, this, options);
  },
  paintText: function (text, {x, y, fillColor, fontSize, letterSpacing}) {
    text = text.toUpperCase();
    const pixelEvents = text2PixelEvents(text, x, y, fontSize, fillColor, letterSpacing);
    _paintPixels(pixelEvents, this, { fillColor: fillColor }); // Draw the text
  }

}


export default ayyo;