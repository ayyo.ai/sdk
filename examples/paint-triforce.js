import AYYO from '../index.js';
let bobbyBot;
async function go () {

  // Creates a new AYYO.Bot client
  let bobbyBot = new AYYO.Bot({
    nickname: 'Zelda',
    gameMode: 'MELEE_FFA', // enum, default MELEE_FFA
    region: 'Washington_DC', // enum, default Washington_DC, add ping test to auto-connect to region
    gameId: 'ayyo-wars', // custom game id connection string, optional
    apiKey: '0xAaAaaaAAaaaAaaeEEeeEeeEe89a9EF980AdcA1fC', // your public ETH address
    ayyoKey: 'top-secret-ayyo-key', // your ayyo.gg API key (optional)
    // wsConnectionString may be set manually if you know server IP
    // this is used for local development, not production!
    // wsConnectionString: 'ws://192.168.1.80:8888'
  });

  // Listens for any errors from the client
  bobbyBot.on('error', function(err){
    throw err;
  });


  // Listens for any errors from the client
  bobbyBot.on('connected', async function(err){
    console.log('connected to server, starting to paint');
    setTimeout(function(){

      let center = {
        "x": 0,
        "y": 0,
      };
      let pixels = sierpinskiTriangle(6, 0, 0, 666, 6, 0x00ff00);
      console.log('apainting', pixels)
      bobbyBot.paintPixels(pixels, {
        fillColor: 'random',
        startX: center.x + 666,
        startY: center.y + 666
      });
    }, 1000)

  });

  // Connects the bot to a random room
  await bobbyBot.connect();

  await bobbyBot.join('Earth'); // pick any known planet as your starting faction

  console.log('whoami:', bobbyBot.currentPlayer);
  

}

go();


function sierpinskiTriangle(level, x, y, size, width, fillColor) {
  if (level === 0) {
    // Base case: return a single pixel at the center of the triangle
    return [{ x, y, fillColor, height: width, width: width, duration: 0 }];
  } else {
    // Recursive case: divide the triangle into three smaller triangles
    const halfSize = size / 2;
    const leftX = x - halfSize;
    const rightX = x + halfSize;
    const topY = y - halfSize * Math.sqrt(3) / 2;
    const bottomY = y + halfSize * Math.sqrt(3) / 2;
    const events = [];

    // Recursively generate the smaller triangles and concatenate the arrays of pixel events
    events.push(...sierpinskiTriangle(level - 1, x, topY, halfSize, width, fillColor));
    events.push(...sierpinskiTriangle(level - 1, leftX, bottomY, halfSize, width, fillColor));
    events.push(...sierpinskiTriangle(level - 1, rightX, bottomY, halfSize, width, fillColor));

    // Return the concatenated array of pixel events
    return events;
  }
}
