import AYYO from '../index.js';

async function go () {

  // Creates a new AYYO.Bot client
  let bobbyBot = new AYYO.Bot({
    nickname: 'Bobby_Spinna',
    gameMode: 'MELEE_FFA', // enum, default MELEE_FFA
    region: 'Washington_DC', // enum, default Washington_DC, add ping test to auto-connect to region
    gameId: 'ayyo-wars', // custom game id connection string, optional
    apiKey: '0xAaAaaaAAaaaAaaeEEeeEeeEe89a9EF980AdcA1fC', // your public ETH address
    ayyoKey: 'top-secret-ayyo-key', // your ayyo.gg API key (optional)
    // wsConnectionString may be set manually if you know server IP
    // this is used for local development, not production!
    // wsConnectionString: 'ws://192.168.1.80:8888'
  });

  // Listens for any errors from the client
  bobbyBot.on('error', function(err){
    throw err;
  });

  // Listens for each gamestate tick event ( 33.33 FPS, 40ms fixed time step)
  let lastTime = 0;
  bobbyBot.on('gamestate', function(gamestate){
    let now = new Date().getTime();
    let processedTime = now - lastTime;
    //console.log(processedTime, gamestate.snapshot.state.length, gamestate.snapshot.id,  now - gamestate.snapshot.time);
    let me = gamestate.snapshot.state.find(function(state){
      return state.id === bobbyBot.currentPlayer;
    })
    if (me) {
      console.log(me)
    }
    //console.log(gamestate.snapshot.state)
    lastTime = now;
  });

  // Listens for any errors from the client
  bobbyBot.on('connected', async function(err){
    // Joins the default game room
    console.log('connected to server');

    // Emit player controller input to the server
    // ( see documentation for all available inputs )
    setInterval(function(){
      // Moves player forward while auto-firing weapon
      // This uses setInterval to send inputs every 100ms as an example
      // More advanced AI bots would call sendInputs inside the bobbyBot.on('gamestate', function(gamestate){}) event handler
      bobbyBot.sendInputs({ RIGHT: true, primaryWeapon: true });
    }, 100);


  });

  // Connects the bot to a random room
  await bobbyBot.connect();
  await bobbyBot.join('Earth');

  console.log('whoami:', bobbyBot.currentPlayer);

}

go();
