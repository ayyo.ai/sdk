import AYYO from '../index.js';
let bobbyBot;
async function go () {

  // Creates a new AYYO.Bot client
  let bobbyBot = new AYYO.Bot({
    nickname: 'Mario',
    gameMode: 'MELEE_FFA', // enum, default MELEE_FFA
    region: 'Washington_DC', // enum, default Washington_DC, add ping test to auto-connect to region
    gameId: 'ayyo-wars', // custom game id connection string, optional
    apiKey: '0xAaAaaaAAaaaAaaeEEeeEeeEe89a9EF980AdcA1fC', // your public ETH address
    ayyoKey: 'top-secret-ayyo-key', // your ayyo.gg API key (optional)
    // wsConnectionString may be set manually if you know server IP
    // this is used for local development, not production!
    // wsConnectionString: 'ws://192.168.1.80:8888'
  });

  // Listens for any errors from the client
  bobbyBot.on('error', function(err){
    throw err;
  });

  // Listens for each gamestate tick event ( 33.33 FPS, 40ms fixed time step)
  bobbyBot.on('gamestate', function(gamestate){
    let me = gamestate.snapshot.state.find(function(state){
      return state.id === bobbyBot.currentPlayer;
    })
    if (me) {
     // console.log('me', me)
    }
  });

  // Listens for any errors from the client
  bobbyBot.on('connected', async function(err){
    console.log('connected to server, paintint text');

    let text = `AYYO, let's \nPAINT !`;

    setTimeout(function(){
      bobbyBot.paintText(text, {
        fontSize: 22,
        fillColor: 0x00ff00
      });
    }, 1000)

  });

  // Connects the bot to a random room
  await bobbyBot.connect();

  await bobbyBot.join('Earth'); // pick any known planet as your starting faction

  console.log('whoami:', bobbyBot.currentPlayer);

}

go();

function getRandomHexColor() {
  // Generate a random number between 0 and 16777215 (ffffff in hexadecimal)
  const randomNum = Math.floor(Math.random() * 16777215);
  return randomNum;
}

