import AYYO from '../index.js';
import Table from 'cli-table';

async function go() {

  // Creates a new AYYO.Bot client
  let bobbyBot = new AYYO.Bot({
    nickname: 'Bobby',
    gameMode: 'MELEE_FFA', // enum, default MELEE_FFA
    region: 'Washington_DC', // enum, default Washington_DC, add ping test to auto-connect to region
    gameId: 'ayyo-wars', // custom game id connection string, optional
    apiKey: '0xAaAaaaAAaaaAaaeEEeeEeeEe89a9EF980AdcA1fC', // your public ETH address
    ayyoKey: 'top-secret-ayyo-key', // your ayyo.gg API key (optional)
    // wsConnectionString may be set manually if you know server IP
    // this is used for local development, not production!
    // wsConnectionString: 'ws://192.168.1.80:8888'
  });

  // Listens for any errors from the client
  bobbyBot.on('error', function (err) {
    throw err;
  });

  bobbyBot.on('gamestate', function (gamestate) {
    // Show some useful info about the gamestate
    // You could also console.log the entire gamestate here
    // console.log('getting gamestate event', gamestate)

    let currentSnapshotId = gamestate.snapshot.id;
    let currentSnapshotTime = new Date(gamestate.snapshot.time);

    console.log(currentSnapshotId, 'Date/Time', currentSnapshotTime, 'States', gamestate.snapshot.state.length)
    console.log('bobbyBot.currentPlayer', bobbyBot.currentPlayer)
    // Perform arbitrary logic on gamestate array
    // Each item contains a state which represents a gamestate entity
    gamestate.snapshot.state.forEach(function (state) {
      // TODO: cleanup view so it's more table like

      // If player is killed, exit ( for now )
      if (state.type === 'EVENT_KILL') {
        console.log('YOU DIED', bobbyBot.currentPlayer)
        if (state.kind === bobbyBot.currentPlayer) {
          // This is the opposite of the reward function
          process.exit();
        }
      }

      if (state.id === bobbyBot.currentPlayer) {

        //var Table = require('cli-table');
        // instantiate
        var table = new Table({
          head: ['id', 'x', 'y', 'score', 'health', 'energy']
          , colWidths: [20, 10, 10, 10, 10, 10]
        });

        // table is an Array, so you can `push`, `unshift`, `splice` and friends
        table.push(
            [state.id + ' ( YOU )', state.x.toFixed(3), state.y.toFixed(3), state.score.toFixed(3), state.health.toFixed(3), state.energy.toFixed(3)]
        );
        console.log(table.toString());
      }
      //console.log(state.id, state.x, state.y);
    });

    // 
    // Set any of the available control input values to true or false
    bobbyBot.sendInputs({
      UP: true,
      RIGHT: true,
      primaryWeapon: true
    });

  });

  // Connects the bot to a random room
  await bobbyBot.connect();

  await bobbyBot.join('Earth'); // pick any known planet as your starting faction

}

go();


