import AYYO from '../index.js';
let bobbyBot;
async function go () {

  // Creates a new AYYO.Bot client
  let bobbyBot = new AYYO.Bot({
    nickname: 'Doctor_Spirograph',
    gameMode: 'MELEE_FFA', // enum, default MELEE_FFA
    region: 'Washington_DC', // enum, default Washington_DC, add ping test to auto-connect to region
    gameId: 'ayyo-wars', // custom game id connection string, optional
    apiKey: '0xAaAaaaAAaaaAaaeEEeeEeeEe89a9EF980AdcA1fC', // your public ETH address
    ayyoKey: 'top-secret-ayyo-key', // your ayyo.gg API key (optional)
    // wsConnectionString may be set manually if you know server IP
    // this is used for local development, not production!
    // wsConnectionString: 'ws://192.168.1.80:8888'
  });
  
  // Listens for any errors from the client
  bobbyBot.on('error', function(err){
    throw err;
  });

  // Listens for each gamestate tick event ( 33.33 FPS, 40ms fixed time step)
  bobbyBot.on('gamestate', function(gamestate){
    let me = gamestate.snapshot.state.find(function(state){
      return state.id === bobbyBot.currentPlayer;
    })
    if (me) {
     // console.log('me', me)
    }
  });

  // Listens for any errors from the client
  bobbyBot.on('connected', async function(err){
    console.log('connected to server');

    const width = 1666;
    const height = 1666;
    const maxIterations = 100;
    let size = 33.33;
    
    const pixels = generateSpiralPixels(width, height, maxIterations, size)
    
    bobbyBot.paintPixels(pixels, {
      fillColor: 'random',
      startX: 1666,
      startY: -666
    });

  });

  // Connects the bot to a random room
  await bobbyBot.connect();

  await bobbyBot.join('Earth'); // pick any known planet as your starting faction


  console.log('whoami:', bobbyBot.currentPlayer);

  
}

go();


function generateSpiralPixels(width, height, numTurns, size) {
  const centerX = width / 2;
  const centerY = height / 2;
  const radiusStep = Math.min(width, height) / (2 * numTurns);
  let currentRadius = radiusStep;
  let currentAngle = 0;
  const pixels = [];
  while (currentRadius < Math.min(width, height) / 2) {
    const x = Math.round(centerX + currentRadius * Math.cos(currentAngle));
    const y = Math.round(centerY + currentRadius * Math.sin(currentAngle));
    const pixelEvent = {
      isDown: true,
      x: x,
      y: y,
      fillColor: '#ffffff',
      height: size,
      width: size,
      duration: 0
    };
    pixels.push(pixelEvent);
    currentRadius += radiusStep;
    currentAngle += Math.PI / 8;
  }
  return pixels;
}


