![AYYO](https://ayyo.gg/img/ayyo-o.png "AYYO")

# AYYO A.I. Bot Software Development Kit

## *Alpha Edition*

**Create your own [ayyo.gg](https://ayyo.gg) A.I. Bots!**

AYYO fully supports automated agents playing our games and can directly incorporate your A.I. Bots into [ayyo.gg](https://ayyo.gg).

AYYO A.I. bots act as virtual players in the AYYO game. AYYO A.I. Bots subscribe and process the AYYO game state in real-time, responding and sending controller inputs to the AYYO servers.

Players can develop and deploy A.I. behaviors to run autonomously in the cloud or in-game with the flip of a switch in the auto-pilot console. 

This repository provides a Software Development Kit for building AYYO A.I. Bots.

## Installation

```bash
npm install @ayyo.gg/sdk
```

## Usage

```js

import AYYO from '@ayyo.gg/sdk';

async function go () {

  // Creates a new AYYO.Bot client
  let bobbyBot = new AYYO.Bot({
    nickname: 'Bobby',
    apiKey: '0xAaAaaaAAaaaAaaeEEeeEeeEe89a9EF980AdcA1fC', // your public ETH address
    ayyoKey: 'top-secret-ayyo-key' // your ayyo.gg API key (optional)
  });

  // Listens for any errors from the client
  bobbyBot.on('error', function (err) {
    throw err;
  });

  bobbyBot.on('gamestate', function (gamestate) {

    // Perform arbitrary logic on gamestate array
    // Each item contains a state which represents a gamestate entity
    gamestate.snapshot.state.forEach(function (state) {
      console.log(state);
    });

    // 
    // Set any of the available control input values to true or false
    bobbyBot.sendInputs({
      UP: false,
      RIGHT: false,
      primaryWeapon: true
    });

});

  // Connects the bot to a random room
  await bobbyBot.connect();

  // Joins the default game room
  await bobbyBot.join();

}

go();

```

*Note: The Server will reset all input values to `false` on each new gametick.*

### Available Control Inputs

```js

const inputs = {
  UP: false,
  DOWN: false,
  LEFT: false,
  RIGHT: false,
  strafeLeft: false,
  strafeRight: false,
  strafeUp: false,
  strafeDown: false,
  primaryWeapon: false,
  secondaryWeapon: false,
  tertiaryWeapon: false,
  quadWeapon: false,
  pentaWeapon: false,
  hexaWeapon: false,
  specialWeapon: false
};

```

## Examples

See: `./examples` folder

*More examples and A.I. Behaviors are coming soon! If you are interested in helping develop AYYO technology come [make first contact](https://discord.gg/C8uTtc4yDG)*

![AYYO](https://ayyo.gg/img/logo.png "AYYO")
## Copyright ayyo.gg 2023