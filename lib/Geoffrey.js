import WebSocket from 'ws';
import stateArraySchema from './schema.js';
import Float2Int from './Float2Int.js';
import discovery from './discovery.js';
import deltaUpdate from './deltaUpdate.js';
import sleep from 'await-sleep'
import fs from 'fs';

function Geoffrey({ nickname, wsConnectionString, region, gameMode, ayyoKey, apiKey, config, localStorage, token }) {
  // console.log('gconfig', config)
  let self = this;
  self.isConnected = false;
  self.currentPlayer = '';
  self.config = config;
  self.connectionAttempts = 0;
  self.ayyoKey = ayyoKey || null;
  self.apiKey = apiKey || null;

  self.region = region || 'Washington_DC';
  // Remark: currently hard-coded to FOREVER_WAR game mode
  // We'll need to copy / refactor the discovery logic from ayyo.js on client to get other game modes working
  self.gameMode = gameMode || 'FOREVER_WAR';

  
  console.log('AYYO game mode and region', self.gameMode, self.region);
  
  self.wsConnectionString = wsConnectionString || null;
  //self.emitter = emitter;
  if (nickname) {
    self.nickname = nickname;
  }
  self.connection = null
  self.roomId = null;
  self.token = token || null;
  self.lastMessageTime = 0;
  self.ping = new Date().getTime();
  self.messageTimestamps = [];

  if (!self.token && localStorage.token) {
    console.log('setting token from local storage', localStorage.token);
    self.token = localStorage.token;
  }

  return self;
}

Geoffrey.prototype.events = {};

Geoffrey.prototype.on = function onEvent(event, fn) {
  // registers new function on emitter
  this.events[event] = this.events[event] || [];
  this.events[event].push(fn);
}

Geoffrey.prototype.emit = function emitEvent(event, data) {
  if (this.events[event]) {
    this.events[event].forEach(function (fn) {
      fn(data);
    });
  }
}


// TODO: move update logic here from data.game_update event
function handleGameUpdate () {

};

Geoffrey.prototype.connect = async function connect(config) {

  let self = this;
  self.connectionAttempts++;

  //console.log('connecting with config', self.config)
  //console.log('self.region', self.region);
  // console.log('self.gameMode', self.gameMode);

  // Remark: If wsConnectionString is not provided, attempt to get it from discovery
  if (!self.wsConnectionString) {
    if (self.region) {
      // get available servers for region
      let bestAvailableServer = await discovery.getAvailableGameServer(self.region, self.gameMode);
      // console.log("got back etherspace bestAvailableServer", bestAvailableServer);
      
      if (bestAvailableServer && bestAvailableServer.processInfo && bestAvailableServer.processInfo.wsConnectionString) {
        self.wsConnectionString = bestAvailableServer.processInfo.wsConnectionString;
        console.log('AYYO, found best available server', self.wsConnectionString)
      } else {
        if (self.connectionAttempts === 1) {
          console.log('AYYO, no available servers found for game mode and region', self.gameMode, self.region);
        }
        console.log(self.connectionAttempts, 'Etherspace is now autoscaling a new server. Waiting for server...');
        // TODO: call connect again with short delay

        await sleep(1111);
        return self.connect(config);

      }
    }
  } else {
    console.log('AYYO, making a direct connection to', self.wsConnectionString);
  }


  // Remark: This promise represents the server connection logic via wsConnectionString
  // This code will only be reached after a wsConnectionString is provided
  return new Promise(function (resolve, reject) {

    // console.log('trying to connect', self);
    // let ayyoDevToken = localStorage.getItem('ayyo-dev-token');
    let ayyoDevToken = null;

    // TODO: pass in header token for connection, can be apiKey
    let serverConnection;

    if (self.token) {
      serverConnection = self.serverConnection = new WebSocket(self.wsConnectionString, self.token);
    } else {
      serverConnection = self.serverConnection = new WebSocket(self.wsConnectionString);
    }

    // let serverConnection = self.serverConnection = new WebSocket(self.wsConnectionString, 'NPQhUkpB9E'); // TODO: user server IP

    serverConnection.onmessage = function (msg) {
      var data = JSON.parse(msg.data);

      if (data.type === 'acceptedGame') {
        // TODO: namespace token to env, so easy to debug and deploy
        // localStorage.setItem('ayyo-dev-token', data.userId);
        //
        // Do not attempt to upgrade to WebRTC data channel ( for now )
        // In the future we will upgrade to WebTransport protocol if available
        //
        //game.G.pair();
        self.isConnected = true;
        self.currentPlayer = data.userId.toString();
        self.token = data.userId.toString();

        // update the localStorage.json file with new token value
        console.log('updating localStorage.json file with new token value', self.token);
        fs.writeFileSync('./config/localStorage.json', JSON.stringify({ token: self.token }));

        self.emit('connected');
        resolve();
        //game.G.connected = true;
      }

      if (data.event === 'game_update') {
        // convert byte array based on avro schema
        const decodedPlayerState = stateArraySchema.fromBuffer(Buffer.from(data.snapshot.state.data));
        data.snapshot.state = [];

        let propertyNames = ['owner', 'nickname', 'type', 'faction', 'id', 'x', 'y', 'width', 'height', 'radius', 'rotation', 'velocity', 'mass', 'health', 'energy', 'duration', 'velocityX', 'velocityY', 'fillColor', 'kills', 'score', 'brain', 'maxHealth', 'maxEnergy', 'damage', 'energyRegenRate', 'healthRegenRate', 'thrust', 'rotationSpeed', 'modifiers', 'kind'];
        let floatyIntTypes = ['x', 'y', 'width', 'height', 'radius', 'rotation', 'velocity', 'mass', 'health', 'energy', 'duration', 'velocityX', 'velocityY', 'maxHealth', 'maxEnergy', 'damage', 'energyRegenRate', 'healthRegenRate', 'thrust', 'rotationSpeed'];

        decodedPlayerState.forEach(function removeThisFunctionForProtobuf(encodedThing) {
          let thing = {};

          if (Object.keys(thing).length > 0) {
            throw new Error('thing should be empty');
          }

          for (let i = 0; i < propertyNames.length; i++) {
            let key = propertyNames[i];
            //console.log('mapping key', key)
            // dont iterate over prototype
            if (floatyIntTypes.indexOf(key) !== -1 && typeof encodedThing[key] !== 'undefined' && encodedThing[key] !== null) {
              //console.log('f2i', encodedThing.id, key, encodedThing[key]);
              thing[key] = Float2Int.decode(encodedThing[key]);
            } else {
              if (encodedThing[key] !== null) {
                thing[key] = encodedThing[key];
              }
            }
          }

          // do not attempt to delta inflate temporal events, as they have static properties
          let temporalEvents = ['EVENT_EXPLODE', 'EVENT_TELEPORT_OUT', 'EVENT_TELEPORT_IN', 'EVENT_PAINT', 'EVENT_KILL', 'EVENT_MESSAGE'];
          let staticThings = ['ELEMENT', 'ETHER', 'PLANET', 'STARPORT', 'INVISIBLE_WALL', 'TURRET', 'KEY', 'HIVE', 'DOOR'];
          let kinematicThings = ['BULLET'];
      
          if (temporalEvents.indexOf(thing.type) === -1 && 
              staticThings.indexOf(thing.type) === -1 &&
              kinematicThings.indexOf(thing.type) === -1) {
            // TODO: whitelist here instead of blacklist
            // only performing delta-updates on dynamic bodies
            deltaUpdate.inflate(thing);
          }
          data.snapshot.state.push(thing)
        });
        // console.log('emitting gamestate', counter)
        self.emit('gamestate', data);
      }

    };

    // TODO: we could emit this event
    serverConnection.onopen = function (ev) {
      console.log('serverConnection.onopen');
    };

    serverConnection.onerror = function (err) {
      console.log('serverConnection.onerror', err)
      self.isConnected = false;
      reject();
    };
    serverConnection.onclose = function () {
      console.log('serverConnection.onclose')
      self.isConnected = false;
      reject();
    };

  });


}

Geoffrey.prototype.sendCreatorJSON = async function sendInputs(json) {
  let self = this;
  self.sendJSON({ event: 'creator_json', ayyoKey: self.ayyoKey, json: json, ctime: new Date().getTime() });
}

Geoffrey.prototype.sendInputs = async function sendInputs(inputs) {
  let self = this;
  self.sendJSON({ event: 'player_inputs', inputs: inputs, ctime: new Date().getTime() });
}

Geoffrey.prototype.join = async function joinGame(faction) {
  let self = this;
  if (typeof faction === 'undefined') {
    faction = 'Earth'; // default
  }
  // console.log('self.currentPlayer', self.currentPlayer)
  await self.sendJSON({ 
    event: 'player_identified',
    apiKey: self.apiKey,
    nickname: self.nickname,
    faction: faction,
    userId: self.currentPlayer,
    ctime: new Date().getTime()
  });


}

Geoffrey.prototype.sendJSON = function sendJSON(json) {
  let self = this;
  if (this.isConnected) {
    //serverConnection
    //this.connection.write(encoder.encode(JSON.stringify(json)));
    // console.log('sending json', json)
    self.serverConnection.send(JSON.stringify(json));
  } else {
    console.log('warning: Geoffrey attempting to sendJSON while not connected to server.', json)
  }
}



export { Geoffrey };