import { HathoraClient } from "@hathora/client-sdk";
import { encode, decode } from "@msgpack/msgpack";

const encoder = new TextEncoder();

function Geoffrey ({ nickname, config }) {
  console.log('gconfig', config)
  let self = this;
  self.isConnected = false;
  self.currentPlayer = '';
  self.config = config;
  //self.emitter = emitter;
  if (nickname) {
    self.nickname = nickname;
  }
  self.connection = null
  self.roomId = null;
  self.token = null;
  self.lastMessageTime = 0;
  self.ping = new Date().getTime();
  self.messageTimestamps = [];
  return self;
}

Geoffrey.prototype.events = {};

Geoffrey.prototype.on = function onEvent (event, fn) {
  // registers new function on emitter
  this.events[event] = this.events[event] || [];
  this.events[event].push(fn);
}

Geoffrey.prototype.emit = function emitEvent (event, data) {
  if (this.events[event]) {
    this.events[event].forEach(function(fn){
      fn(data);
    });
  }
}

Geoffrey.prototype.connect = async function connect (config) {
  
  let _localStorage;

  //
  // Conditionally requires localStorage shim / polyfill for server
  // This allows localStroage to persist in ./config/localStorage.json file
  //
  if (typeof window !== 'undefined') {
    _localStorage = window.localStorage
  } else {
    // Remark: Issues with vite build always importing localStorage-shim and causing fs failure
    // Need to comment this out for vite build, vite dev still works
    let asyncImport = await import('./localStorage-shim.js');
    _localStorage = asyncImport.default;
    // _localStorage = window.localStorage

  }
  console.log('aaaHathora Client connecting...', self.config)
  const self = this;
  const client = new HathoraClient(self.config.appId);
  console.log('ccc', client)
  // check local storage for token
  //_localStorage.clear();
  //alert('hi')
  let token = _localStorage.getItem('ayyo-hathora-token');
  console.log('found AYYO token', token)
  if (!token) {
    console.log('no token found, creating new login', self.nickname)
    if (self.nickname) {
      token = await client.loginNickname(self.nickname);
    } else {
      token = await client.loginAnonymous();
      
    }
    _localStorage.setItem('ayyo-hathora-token', token);
  }
  
  const player = HathoraClient.getUserFromToken(token);
  console.log('WHOAMI', player)
  self.currentPlayer = player.id;

  // if needed, create a new room
  //const roomId = await client.create(token, new Uint8Array());
  const roomId = self.config.roomId;
  console.log('roomId:', roomId)

  function onError(error) {
    self.isConnected = false;
    console.log('onError CALLED', error);
    self.emit('error', error);
    throw new Error(error)
  }
  
  console.log('attemping to connect', token, roomId)
  const connection = await client.connect(token, roomId, function(data){  
    self.onMessage(data);
  }, onError);

  self.connection = connection;
  self.roomId = roomId;
  self.token = token;
  self.isConnected = true;
  self.emit('connected', { roomId: this.roomId, token: this.token });

  return this;
}

Geoffrey.prototype.sendInputs = async function sendInputs (inputs) {
  let self = this;
  self.sendJSON({ event: 'player_inputs', inputs: inputs, ctime: new Date().getTime() });
}

Geoffrey.prototype.join = async function joinGame (userId) {
  let self = this;
  await self.sendJSON({ event: 'player_identified', nickname: self.nickname, userId: self.currentPlayer, ctime: new Date().getTime() });
}

Geoffrey.prototype.sendJSON = function sendJSON (json) {
  if (this.isConnected) {
    this.connection.write(encoder.encode(JSON.stringify(json)));
  } else {
    console.log('warning: Geoffrey attempting to sendJSON while not connected to server.')
  }
}

let count = 0;

let lastMessage;

let start = performance.now();

Geoffrey.prototype.onMessage = async function (data) {

  let self = this;

  if (typeof lastMessage === 'undefined') {
    lastMessage = new Date().getTime();
  }
  
  let now = new Date().getTime();
  let diff = start = performance.now();
  //let diff = now - lastMessage;
  lastMessage = now;
  if (count % 10 === 0) {
    //console.log('diff', diff)
  }
  
  count++;
  // Add the timestamp to the array
  if (count > 1) {
    // ignore the first timestamp
    self.messageTimestamps.push(diff);
  }
  
  if (self.messageTimestamps.length >= 2) {
    let totalTime = 0;
    if (count % 10 === 0) {
      //console.log(self.messageTimestamps[1] - self.messageTimestamps[0])

    }

    self.messageTimestamps.forEach((t, i) => {
      if (i > 0) {
        totalTime += (t - self.messageTimestamps[i - 1]);
      }
    });

    self.ping = totalTime / (self.messageTimestamps.length - 2);
    // TODO: use config
    // 33.33 hzMS
    self.ping = (self.ping).toFixed(2);
  }

  if (self.messageTimestamps.length >= 120) { // 25 hzMS = 25 fps = 120 seconds = 3 seconds of ping history
    self.messageTimestamps.shift();
  }
  //self.lastMessageTime = now;
  start = performance.now();
  
  // decode the msgpack
  let json = decode(data);
  
  self.emit('gamestate', json);
};

export { Geoffrey };