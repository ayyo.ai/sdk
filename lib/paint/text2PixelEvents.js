import CHAR_PIXELS from './ayyoAlpha.js';

function text2PixelEvents(text, x = 0, y = 0, fontSize = 10, color = 0x0000ff) {
  // console.log('text2PixelEvents', text, x, y, fontSize, color, letterSpacing)
  const pixelEvents = [];
  let letterSpacing = fontSize * 5;
  const pixelSize = fontSize; // set the size of each pixel
  const fontHeight = Math.ceil(fontSize * 1.5); // assume line height is 1.5 times the font size

  let currentX = x;
  let currentY = y;

  for (let i = 0; i < text.length; i++) {
    const character = text.charAt(i);

    if (character === '\n') {
      // Move to the start of the next line
      currentX = x;
      currentY += fontHeight * pixelSize * 1.5;
    } else {
      // Map the character to its pixel representation
      const pixels = CHAR_PIXELS[character];
      if (!pixels) {
        // If the character is not supported, move to the next character
        console.log('Unsupported character: ' + character);
        continue;
      }

      // Calculate the width of the character based on the font size and letter spacing
      const charWidth = Math.ceil((fontSize * 0.6 + letterSpacing) * pixelSize);

      // Draw each pixel in the character
      for (let row = 0; row < fontHeight; row++) {
        for (let col = 0; col < charWidth; col++) {
          if (pixels[row] && pixels[row][col]) {
            // If the pixel is set, add a pixel event to draw it
            pixelEvents.push({
              x: currentX + col * pixelSize,
              y: currentY + row * pixelSize,
              height: pixelSize,
              width: pixelSize,
              duration: 0,
              fillColor: color
            });
          }
        }
      }

      // Move the x position to the right to prepare for the next character
      currentX += charWidth / pixelSize;
    }
  }

  return pixelEvents;
}

export default text2PixelEvents;