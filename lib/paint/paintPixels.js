// takes an array of pixel events and paints them on the canvas
function paintPixels(pixelEvents, bot, options) {
  let i = 0;
  let self = this;

  if (typeof options === 'undefined') {
    options = {};
  }
  if (typeof options.startX === 'undefined') {
    options.startX = 0;
  }
  if (typeof options.startY === 'undefined') {
    options.startY = 0;
  }
  function drawPixel() {
    if (i < pixelEvents.length) {
      const pixelEvent = pixelEvents[i];
      let fillColor = options.fillColor || pixelEvent.fillColor;
      if (fillColor === 'random') {
        fillColor = getRandomHexColor();
      }
      bot.sendInputs({
        PAINT: {
          isDown: true,
          x: pixelEvent.x + options.startX,
          y: pixelEvent.y + options.startY,
          fillColor: fillColor,
          height: pixelEvent.height,
          width: pixelEvent.width,
          duration: pixelEvent.duration
        }
      });
      i++;
      // Set a timeout to call the next pixel event after 333ms
      setTimeout(drawPixel, 111);
    }
  }

  drawPixel();
}

function getRandomHexColor() {
  // Generate a random number between 0 and 16777215 (ffffff in hexadecimal)
  const randomNum = Math.floor(Math.random() * 16777215); // could this also be 0xffffff?
  return randomNum;
}

export default paintPixels;