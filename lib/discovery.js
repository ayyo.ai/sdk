import axios from 'axios';

const discovery = {};

discovery.etherspaceEndpoint = 'https://etherspace.ayyo.gg';
//discovery.etherspaceEndpoint = 'http://192.168.1.80';

const instance = axios.create({
  baseURL: discovery.etherspaceEndpoint,
});

discovery.serverPollCount = 0;

discovery.getAvailableGameServer = async function getAvailableGameServer (region, gameMode) {
  // ping out to etherspace with region information and return a list of available games
  let servers = await ayyoEtherspaceAutoscale(region, gameMode);
  let bestServer;

  if (servers && servers.length > 0) {
    bestServer = servers[0];
  }
  return bestServer;
};

async function ayyoEtherspaceAutoscale(region, mode, startingPlanet) {
  // let url = `${ayyo.etherspaceEndpoint}/api/v1/autoscale/${region}/${mode}/${startingPlanet}`;
  let url = discovery.etherspaceEndpoint + `/api/v1/autoscale/${region}/${mode}`
  console.log("AYYO etherspace autoscale contact", url)
  try {
    const response = await instance.post(url);
    // console.log(response.data);
    return response.data;
  } catch (err) {
    return {};
  }
}

async function ayyoEtherspace(region, ip) {
  //let url = discovery.etherspaceEndpoint + `/api/v1/ayyo/` + region + "?env=dev";
  let url = discovery.etherspaceEndpoint + `/api/v1/ayyo/` + region;

  console.log("ayyo etherspace contact", url)
  try {
    const response = await instance.get(url);
    // console.log(response.data);
    return response.data;
  } catch (err) {
    return {};
  }
}

export default discovery;