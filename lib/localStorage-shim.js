//
// Provides simple local storage shim for running the SDK outside of the browser
// Stores all localStorage data in localStorage.json file ( such as AYYO auth token )
//
const localStorage = {};


export default localStorage;
